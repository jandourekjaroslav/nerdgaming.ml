<?php session_start();




?>
<!DOCTYPE html> 
<html>
	<head>
		<title>Nerdgaming</title>
		<meta charset="utf-8">	
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div id="banner"></div>

		<nav id="navbar">
			<a href="index.php">News</a>
			<a href="gallery.php">Gallery</a>
			<a href="events.php">Events</a>
		</nav>	<?php
			
			if (!isset($_SESSION["logged"])){		?>
<!-- Login/Logout   -->

		<div id="login">

			<form action="index.php" method="POST">
				Username: <input type="text" name="uname">
				 Password: <input type="password" name="upass">
				<input type="submit" name="Login" value="Login">
				<button type="button" id="reg">Register</button>
			</form>
		</div><?php }else {
			?>
			<div id="login">
				<form action="index.php" method="POST">
					Logged in as <?php echo ($_SESSION["user"]);?> <input type="submit" name="Logout" value="Logout">
				</form>
			</div>
			<?php } ?>
<!-- Login/Logout END  -->

		<div id="main">
			<h1>Events this week</h1>
			<?php include "phpwork.php" ?>
<!-- Table pro zobrazování eventů   -->
			
		<div class="content" id="eventdiv">
			<table>
				<tr id="mon"> <?php if(isset($_SESSION["logged"])){	  ?>		<td><button id="signupmon">Sign up</button></td> <?php };if (isset($_SESSION["isadmin"])&&($_SESSION["isadmin"]==true)){ ?>		<td><button id="deletemon">Delete</button></td>	<?php }; ?>	</tr>
				<tr id="tues"> <?php if(isset($_SESSION["logged"])){	  ?>		<td><button id="signuptus">Sign up</button></td> <?php };if (isset($_SESSION["isadmin"])&&($_SESSION["isadmin"]==true)){ ?>		<td><button id="deletetus">Delete</button></td>	<?php }; ?>	</tr>
				<tr id="wed"> <?php if(isset($_SESSION["logged"])){	  ?>		<td><button id="signupwed">Sign up</button></td> <?php };if (isset($_SESSION["isadmin"])&&($_SESSION["isadmin"]==true)){ ?>		<td><button id="deletewed">Delete</button></td>	<?php }; ?>	</tr>
				<tr id="thur"> <?php if(isset($_SESSION["logged"])){	  ?>		<td><button id="signupthur">Sign up</button></td> <?php };if (isset($_SESSION["isadmin"])&&($_SESSION["isadmin"]==true)){ ?> <td><button id="deletethur">Delete</button></td> <?php }; ?>	</tr>
				<tr id="fri"> <?php if(isset($_SESSION["logged"])){	  ?>		<td><button id="signupfri">Sign up</button></td> <?php };if (isset($_SESSION["isadmin"])&&($_SESSION["isadmin"]==true)){ ?>		<td><button id="deletefri">Delete</button></td>	<?php }; ?>	</tr>
				<tr id="sat"> <?php if(isset($_SESSION["logged"])){	  ?>		<td><button id="signupsat">Sign up</button></td> <?php };if (isset($_SESSION["isadmin"])&&($_SESSION["isadmin"]==true)){ ?>		<td><button id="deletesat">Delete</button></td>	<?php }; ?>	</tr>
				<tr id="sun"> <?php if(isset($_SESSION["logged"])){	  ?>		<td><button id="signupsun">Sign up</button></td> <?php };if (isset($_SESSION["isadmin"])&&($_SESSION["isadmin"]==true)){ ?>		<td><button id="deletesun">Delete</button></td>	<?php }; ?>	</tr>


			</table>	
		</div>
<!-- Table pro zobrazování eventů END  -->

<!-- form pro vytváření eventů  -->

		<?php if (isset($_SESSION["isadmin"])&&($_SESSION["isadmin"]==true)){ ?>
		<div class="content"><br>
			<form action="events.php" method="POST">
				Description:
				<input name="description" type="text" required>
				Date:
				<input type="text" name="datum" value="mm/dd/yyyy" required>
				<input type="submit" name="createevent" value="Create Event">


			</form>
<!-- form pro vytváření eventů END -->

		</div>	
			<?php }; ?>
		</div>
		

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="scripts.js" ></script>
	<script type="text/javascript" src="scriptsevent.js" ></script>
	<footer>&copy; 2016 Jaroslav Jandourek 	CVUT FEL-SIT</footer>
	</body>


</html>

