
<!DOCTYPE html> 
<html>

	<head>
	
		
		<title>Nerdgaming</title>
		<meta charset="utf-8">	
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div id="banner"></div>

		<nav id="navbar">
			<a href="index.php">News</a>
			<a href="gallery.php">Gallery</a>
			<a href="events.php">Events</a>
		</nav>	<?php
			
			if (!isset($_SESSION["logged"])){		?>
<!-- Login/Logout  -->

		<div id=login>

			<form action="index.php" method="POST">
				Username: <input type="text" name="uname" required>
				 Password: <input type="password" name="upass" required>
				<input type="submit" name="Login" value="Login">
				<button type="button" id="reg">Register</button>
			</form>
		</div><?php }else {
			?>
			<div id="login">
				<form action="index.php" method="POST">
					Logged in as <?php echo ($_SESSION["user"]);?> <input type="submit" name="Logout" value="Logout">
				</form>
			</div>
			<?php } ?>
<!-- Login/Logout END  -->

<!-- Registrační formulář  -->

		<div id="main">
			<div id="regiwrap">
			<h1>Registration</h1>
			<?php include "phpwork.php";
				
			 ?>
			<form action ="register.php" method="POST">
				<table>
					<tr>
						<td>*Username:</td>
						<td> <input type="text" name="unamereg" required> </td>
					</tr>
					<tr>
						<td>*Password:</td>
						<td> <input type="password" name="upassreg1" required> </td>
					</tr>
					<tr>
						<td>*Confirm Password:</td>
						<td> <input type="password" name="upassreg2" required> </td>
					</tr>
					<tr>
						<td>*Email:</td>
						<td> <input type="email" name="uemail" required> </td>
					</tr>
					<tr>
						<td>Your welcome message:</td>
						<td> <textarea name="umessage" id="mes"> </textarea> </td>
					</tr>
					<tr><td></td>
						<td><input type="submit" name="Register" value="Register"></td>
					</tr>
				</table>



			</form>


			</div>
		</div>
		
<!-- Registrační formulář  END -->

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="scripts.js" ></script>
	<footer>&copy; 2016 Jaroslav Jandourek 	CVUT FEL-SIT</footer>
	</body>

</html>

