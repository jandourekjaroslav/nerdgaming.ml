<?php 
# Objekt User- Uživatel obsahuje veškerá data o uživateli, serializovaná data ukládaná do textového souboru users.txt
class User{
	public $name;
	public $pword;
	public $isadmin;
	public $email;
	public $message;

	public function __construct($username,$pass,$uemail,$mes){
		$this->name=$username;
		$this->pword= password_hash($pass,PASSWORD_DEFAULT);
		$this->isadmin=false;
		$this->email=$uemail;
		$this->message=$mes;


	}


}
# Objekt User- END

# Objekt Event-Akce obsahuje veškerá data o akcích, serializovaná data ukládaná do textového souboru events.txt

class Event{
	public $datum;
	public $description;
	public $players;

	public function __construct($datum,$description){
		$this->datum=$datum;
		$this->description=$description;
		$this->players=array();
	}

}

# Objekt Event- END



# Část kódu obsluhující mazání eventu 
	
	if(isset($_POST["dayd"])){
		$day=$_POST["dayd"];
		$testtime=strtotime("next $day");
		$file=fopen("events.txt", "r");
		$found=false;
		while(!feof($file)){
		$event=unserialize(fgets($file));
		if (is_object($event)){if (strtotime($event->datum)==$testtime){
			$found=true;
			break;
		}}
	}
	fclose($file);
	$file=fopen("events.txt", "r");
	$fileread=(fread($file, filesize("events.txt")));
	fclose($file);
	$eventupdated="".PHP_EOL;
	
	str_replace(serialize($event), serialize($eventupdated), $fileread);
	$file=fopen("events.txt", "w");
	fwrite($file, $fileread);
	fclose($file);

	echo "Event deleted";

	};
#Delete Event end 

# Část kódu vracející eventy k zobrazení pro následující pondělí-neděli 

	if ((isset($_POST["action"]))&&($_POST["action"]=="monday")){
		$testtime=strtotime("next monday");
		$file=fopen("events.txt", "r");
		$found=false;
		while(!feof($file)){
		$event=unserialize(fgets($file));
		if (is_object($event)){if (strtotime($event->datum)==$testtime){
			$found=true;
			break;
		}}
	}
	fclose($file);
	if ($found){
		$text="Signed up: <ul>";
		for ($i=0; $i <count($event->players) ; $i++) { 
			$player=$event->players[$i];
			$text=$text."<li>$player</li>";		
		}
		$text=$text."</ul>";
		echo "<td>$event->datum     </td><td>$event->description</td><td>$text</td>";
	}else{
		echo "disable";
	};


	};

	if ((isset($_POST["action"]))&&($_POST["action"]=="tuesday")){
		$testtime=strtotime("next tuesday");
		$file=fopen("events.txt", "r");
		$found=false;
		while(!feof($file)){
		$event=unserialize(fgets($file));
		if (is_object($event)){if (strtotime($event->datum)==$testtime){
			$found=true;
			break;
		}}
	}
	fclose($file);
	if ($found){
		$text="Signed up: <ul>";
		for ($i=0; $i <count($event->players) ; $i++) { 
			$player=$event->players[$i];
			$text=$text."<li>$player</li>";		
		}
		$text=$text."</ul>";
		echo "<td>$event->datum     </td><td>$event->description</td><td>$text</td>";
	}else{
		echo "disable";
	};




	};

	if ((isset($_POST["action"]))&&($_POST["action"]=="wednesday")){
		$testtime=strtotime("next wednesday");
		$file=fopen("events.txt", "r");
		$found=false;
		while(!feof($file)){
		$event=unserialize(fgets($file));
		if (is_object($event)){if (strtotime($event->datum)==$testtime){
			$found=true;
			break;
		}}
	}
	fclose($file);
	if ($found){
		$text="Signed up: <ul>";
		for ($i=0; $i <count($event->players) ; $i++) { 
			$player=$event->players[$i];
			$text=$text."<li>$player</li>";		
		}
		$text=$text."</ul>";
		echo "<td>$event->datum     </td><td>$event->description</td><td>$text</td>";
	}else{
		echo "disable";
	};




	};

	if ((isset($_POST["action"]))&&($_POST["action"]=="thursday")){

		$testtime=strtotime("next thursday");
		$file=fopen("events.txt", "r");
		$found=false;
		while(!feof($file)){
		$event=unserialize(fgets($file));
		if (is_object($event)){if (strtotime($event->datum)==$testtime){
			$found=true;
			break;
		}}
	}
	fclose($file);
	if ($found){
		$text="Signed up: <ul>";
		for ($i=0; $i <count($event->players) ; $i++) { 
			$player=$event->players[$i];
			$text=$text."<li>$player</li>";		
		}
		$text=$text."</ul>";
		echo "<td>$event->datum     </td><td>$event->description</td><td>$text</td>";
	}else{
		echo "disable";
	};



	};

	if ((isset($_POST["action"]))&&($_POST["action"]=="friday")){
		$testtime=strtotime("next friday");
		$file=fopen("events.txt", "r");
		$found=false;
		while(!feof($file)){
		$event=unserialize(fgets($file));
		if (is_object($event)){if (strtotime($event->datum)==$testtime){
			$found=true;
			break;
		}}
	}
	fclose($file);
	if ($found){
		$text="Signed up: <ul>";
		for ($i=0; $i <count($event->players) ; $i++) { 
			$player=$event->players[$i];
			$text=$text."<li>$player</li>";		
		}
		$text=$text."</ul>";
		echo "<td>$event->datum     </td><td>$event->description</td><td>$text</td>";
	}else{
		echo "disable";
	};





	};

	if ((isset($_POST["action"]))&&($_POST["action"]=="saturday")){
		$testtime=strtotime("next saturday");
		$file=fopen("events.txt", "r");
		$found=false;
		while(!feof($file)){
		$event=unserialize(fgets($file));
		if (is_object($event)){if (strtotime($event->datum)==$testtime){
			$found=true;
			break;
		}}
	}
	fclose($file);
	if ($found){
		$text="Signed up: <ul>";
		for ($i=0; $i <count($event->players) ; $i++) { 
			$player=$event->players[$i];
			$text=$text."<li>$player</li>";		
		}
		$text=$text."</ul>";
		echo "<td>$event->datum     </td><td>$event->description</td><td>$text</td>";
	}else{
		echo "disable";
	};




	};

	if ((isset($_POST["action"]))&&($_POST["action"]=="sunday")){
		$testtime=strtotime("next sunday");
		$file=fopen("events.txt", "r");
		$found=false;
		while(!feof($file)){
		$event=unserialize(fgets($file));
		if (is_object($event)){if (strtotime($event->datum)==$testtime){
			$found=true;
			break;
		}}
	}
	fclose($file);
	if ($found){
		$text="Signed up: <ul>";
		for ($i=0; $i <count($event->players) ; $i++) { 
			$player=$event->players[$i];
			$text=$text."<li>$player</li>";		
		}
		$text=$text."</ul>";
		echo "<td>$event->datum     </td><td>$event->description</td><td>$text</td>";
	}else{
		echo "disable";
	};




	};

# Část kódu vracející eventy k zobrazení pro následující pondělí-neděli -END 

# Část kódu pro vytváření nových eventů

	if (isset($_POST["createevent"])){
		$description=htmlspecialchars($_POST["description"]);
		$date=htmlspecialchars($_POST["datum"]);
		$event= new Event($date,$description);
		$serialevent=serialize($event).PHP_EOL;
		$file=fopen("events.txt", "a");
		fwrite($file, $serialevent);
		fclose($file);

	};
# Část kódu pro vytváření nových eventů-END

# Část kódu pro vytváření nových Postů do News 


	if (isset($_POST["createpost"])){
		$text=htmlspecialchars($_POST["content"]);
		$title=htmlspecialchars($_POST["title"]);
		$textact="<div class='content'><article><h2>$title</h2> <p>$text</p></article></div>";
		$file=fopen("articlecheck.txt", "r");
		$num=unserialize(fgets($file))+1;
		fclose($file);
		$file=fopen("posts/A$num.txt", "w");
		fwrite($file, $textact);
		fclose($file);
		$file=fopen("articlecheck.txt", "w");
		fwrite($file, serialize($num));
		fclose($file);




	};
# Část kódu pro vytváření nových Postů do News -END

# Část kódu pro vytváření nových komentářů

	if (isset($_POST["postcom1"])){
		$file=fopen("posts/C1.txt", "a");
		$user=$_SESSION["user"];
		$comment=htmlspecialchars($_POST["commentp"]);
		$text="<div><h4>$user</h4><p>$comment</p></div>";
		fwrite($file, $text);
		fclose($file);
	};
	if (isset($_POST["postcom2"])){
		$file=fopen("posts/C2.txt", "a");
		$user=$_SESSION["user"];
		$comment=htmlspecialchars($_POST["commentp"]);
		$text="<div><h4>$user</h4><p>$comment</p></div>";
		fwrite($file, $text);
		fclose($file);
	};
	if (isset($_POST["postcom3"])){
		$file=fopen("posts/C3.txt", "a");
		$user=$_SESSION["user"];
		$comment=htmlspecialchars($_POST["commentp"]);
		$text="<div><h4>$user</h4><p>$comment</p></div>";
		fwrite($file, $text);
		fclose($file);
	};
	if (isset($_POST["postcom4"])){
		$file=fopen("posts/C4.txt", "a");
		$user=$_SESSION["user"];
		$comment=htmlspecialchars($_POST["commentp"]);
		$text="<div><h4>$user</h4><p>$comment</p></div>";
		fwrite($file, $text);
		fclose($file);
	};
	if (isset($_POST["postcom5"])){
		$file=fopen("posts/C5.txt", "a");
		$user=$_SESSION["user"];
		$comment=htmlspecialchars($_POST["commentp"]);
		$text="<div><h4>$user</h4><p>$comment</p></div>";
		fwrite($file, $text);
		fclose($file);
	};
	if (isset($_POST["postcom6"])){
		$file=fopen("posts/C6.txt", "a");
		$user=$_SESSION["user"];
		$comment=htmlspecialchars($_POST["commentp"]);
		$text="<div><h4>$user</h4><p>$comment</p></div>";
		fwrite($file, $text);
		fclose($file);
	};
	if (isset($_POST["postcom7"])){
		$file=fopen("posts/C7.txt", "a");
		$user=$_SESSION["user"];
		$comment=htmlspecialchars($_POST["commentp"]);
		$text="<div><h4>$user</h4><p>$comment</p></div>";
		fwrite($file, $text);
		fclose($file);
	};
# Část kódu pro vytváření nových komentářů END

# Část kódu pro mazání fotek z galerie

	if (isset($_POST["index"])){
		if ($_SESSION["isadmin"]==true) {
			$id=$_POST["index"];
			$filename="gallery/$id.txt";
			$file1=fopen($filename, "w");
			fclose($file1);
			rename($filename, "gallery/37.txt");
			for ($i=$id+1; $i <37 ; $i++) {
				$y=$i-1; 
				rename("gallery/$i.txt", "gallery/$y.txt");	
				
			}
			rename("gallery/37.txt", "gallery/36.txt");
			$file=fopen("gallerycheck.txt","r");
			$val=unserialize(fgets($file));
			fclose($file);
			$file=fopen("gallerycheck.txt","w");
			fwrite($file, serialize($val-1));
			fclose($file);
			echo "Picture will be deleted shortly";
		}else{
			echo "Access denied";
		}

# Část kódu pro mazání fotek z galerie-END

# Část kódu pro mazání postů a všech jeho komentářů z NEWS


	};
	if (isset($_POST["deletepost"])){
		if ($_SESSION["isadmin"]==true){
		$numb=$_POST["posttodelete"];
		$file=fopen("posts/A$numb.txt", "w");
		$comfile=fopen("posts/C$numb.txt", "w");
		fclose($file);
		fclose($comfile);
		rename("posts/A$numb.txt", "posts/A8.txt");
		rename("posts/C$numb.txt","posts/C8.txt");
		for ($i=$numb+1; $i <8 ; $i++) {
				$y=$i-1; 
				rename("posts/A$i.txt", "posts/A$y.txt");
				rename("posts/C$i.txt", "posts/C$y.txt");	
				
			}
		rename("posts/A8.txt", "posts/A7.txt");
		rename("posts/C8.txt", "posts/C7.txt");
		$file=fopen("articlecheck.txt", "r");
		$id=unserialize(fgets($file))-1;
		fclose($file);
		$file=fopen("articlecheck.txt", "w");
		fwrite($file, serialize($id));
		fclose($file);
		}else{
			echo "Access denied";
		};		




	};
# Část kódu pro mazání postů a všech jeho komentářů z NEWS-END

# Část kódu pro zjištění počtu postů v News	pro delete formulář

	if ((isset($_POST["action"]))&&($_POST["action"]=="postdelcontrol")){
		$file=fopen("articlecheck.txt", "r");
		$numb=unserialize(fgets($file));
		echo ($numb);
		fclose($file);




	};
# Část kódu pro zjištění počtu postů v News pro delete formulář-END	

# Část kódu pro zjištění počtu fotek v Galerii

	if ((isset($_POST["action"]))&&($_POST["action"]=="findgalpost")){
		$file=fopen("gallerycheck.txt", "r");
		$numb=unserialize(fgets($file));
		echo $numb;
		fclose($file);

	};
# Část kódu pro zjištění počtu fotek v Galerii-END

# Část kódu pro zjištění počtu postů v News	pro zobrazení

	if ((isset($_POST["action"]))&&($_POST["action"]=="article")){
		$file=fopen("articlecheck.txt", "r");
		echo (unserialize(fgets($file)));
		fclose($file);

	};	
# Část kódu pro zjištění počtu postů v News	pro zobrazení-END

# Část kódu obsluhující Upload fotek do Galerie
	
	if (isset($_POST["Upload"])){
		$nadpis=htmlspecialchars($_POST["nadpis"]);
		$popisek=htmlspecialchars($_POST["popisek"]);
		$target_dir="images/";
		$target_file=$target_dir.basename($_FILES["imageup"]["name"]);
		$uploadOk=1;
		$imageFileType=pathinfo($target_file,PATHINFO_EXTENSION);
		$check=getimagesize($_FILES["imageup"]["tmp_name"]);
		if ($check!==false){
			$uploadOk=1;
		}else{$uploadOk=0;};


		if (file_exists($target_file)){
			$uploadOk=0;
			echo "File already exists ";
		};
		if ($_FILES["imageup"]["size"]>50000000){
			echo "file too big ".PHP_EOL;
			$uploadOk=0;
		};
		if ($uploadOk==0){
			echo "File was not uploaded ";
		}else{
			move_uploaded_file($_FILES["imageup"]["tmp_name"], $target_file);
			$file=fopen("gallerycheck.txt", "r");
			$imagenum=unserialize(fgets($file));
			fclose($file);
			$file=fopen("gallerycheck.txt", "w");
			$imagenum++;
			fwrite($file, serialize($imagenum));
			fclose($file);
			$imageindex=$imagenum.".txt";
			$pageindex=$imageindex%6;
			$galfile=fopen("gallery/$imageindex", "w");
			$text="<img class='galleryimg' href='$target_file'  src='$target_file' alt='$nadpis'>
<h2>$nadpis</h2>
<p> $popisek</p>


";
			fwrite($galfile, $text);

			fclose($galfile);
		}
	}
# Část kódu obsluhující Upload fotek do Galerie -END

#Logout
	if (isset($_POST["Logout"])){
		unset($_SESSION["logged"]);
		session_destroy();

	};
#Logout-END

#Login

	if (isset($_POST["Login"])){
		$username=correct_input($_POST["uname"]);
		$password=correct_input($_POST["upass"]);		
		$file=fopen("users.txt", "r");
		$found=false;
		
		while (!feof($file)){
			$line=fgets($file);
			
			$line=unserialize($line);
			if ($line->name ==$username){
				$found=true;
				break;
			}
		};
		$admin=$line->isadmin;
		$mes=$line->message;
		
		if ($found==false){
			echo("Wrong username");

		}elseif (!password_verify($password,$line->pword)) {
			echo "Wrong password!";
		}else{
			echo "$mes";
			$_SESSION["logged"]=true;
			$_SESSION["user"]=$username;
			$_SESSION["isadmin"]=$admin;
			$permauser="$username";
			

		}
		fclose($file);
	}
#Login-END

#Registrace
	if (isset($_POST["Register"])){
		$regusername=correct_input($_POST["unamereg"]);
		$regpassword1=correct_input($_POST["upassreg1"]);
		$regpassword2=correct_input($_POST["upassreg2"]);
		$regemail=correct_input($_POST["uemail"]);
		$welmessage=htmlspecialchars($_POST["umessage"]);

		if ((checkuname($regusername))&&($regpassword1==$regpassword2)){
			$user=new User($regusername,$regpassword1,$regemail,$welmessage);
			$serialuser=serialize($user).PHP_EOL;
			$file=fopen("users.txt", "a+");
			fwrite($file, $serialuser);
			fclose($file);
			
		}else{
			if ($regpassword1==$regpassword2){
			echo "Username too short or already exists";
			}else{
			echo "Passwords do not match";
			}
		}


	}

#Registrace-END

#Kontrola Už.jména

function checkuname($name=""){
	$file=fopen("users.txt", "r");
	$exists=true;
	while (!feof($file)){
			$line=fgets($file);
			
			$line=unserialize($line);
			if ($line->name ==$name){
				$exists=false;
				break;
			}
		};
	fclose($file);		


	return ((strlen($name)>=6)&&($exists));


}
#Kontrola Už.jména-END

#Úprava inputu - slashes , specialchars, mezery
	
function correct_input($data){
	$data=trim($data);
	$data=stripcslashes($data);
	$data=htmlspecialchars($data);
	return $data;

}
#Úprava inputu - slashes , specialchars, mezery -END
?>