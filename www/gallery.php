<?php session_start();




?>

<!DOCTYPE html> 
<html>
	<head>
		<title>Nerdgaming</title>
		<meta charset="utf-8">	
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div id="banner"></div>

		<nav id="navbar">
			<a href="index.php">News</a>
			<a href="gallery.php">Gallery</a>
			<a href="events.php">Events</a>
		</nav>	<?php
			
			if (!isset($_SESSION["logged"])){		?>
		<div id=login>

			<form action="index.php" method="POST">
				Username: <input type="text" name="uname" required>
				 Password: <input type="password" name="upass" required>
				<input type="submit" name="Login" value="Login">
				<button type="button"  id="reg">Register</button>
			</form>
		</div><?php }else {
			?>
			<div id="login">
				<form action="index.php" method="POST">
					Logged in as <?php echo ($_SESSION["user"]);?> <input type="submit" name="Logout" value="Logout">
				</form>
			</div>
			<?php } ?>
		<div id="main">
			<div id="gallerywrap">				
				<h1>Gallery</h1>
					<?php include "phpwork.php";
						if ((isset($_SESSION["isadmin"]))&&($_SESSION["isadmin"])){
					 ?>
					<form action="" method="POST" enctype="multipart/form-data">
						Select Image to upload :
						<input type="file" name="imageup" id="imageup">
						Title:
						<input type="text" name="nadpis" required>
						Description:
						<input type="text" name="popisek" required>
						<input type="submit" value="Upload" name="Upload"> 

					</form>
					<?php }?>
<!-- Table zobrazující obrázky a pro Admina tlačítka delete   -->

					<table id="gallerytable">
						<tr>
							<td id="image1">
								
								
							</td>
							<td id="image2">
								
								
								
							</td>
							<td id="image3">
								
								
								
							</td>
						</tr>
						<?php if ((isset($_SESSION["isadmin"]))&&($_SESSION["isadmin"])){?>
						<tr>
							<td><button id="delete1" value="1">Delete</button></td>
							<td><button id="delete2" value="2">Delete</button></td>
							<td><button id="delete3" value="3">Delete</button></td>

						</tr>
						<?php };?>

						<tr>
							<td id="image4">
								
								
							</td>
							<td id="image5">
								
								
								
							</td>
							<td id="image6">
								
								
								
							</td>
						</tr>
						<?php if ((isset($_SESSION["isadmin"]))&&($_SESSION["isadmin"])){?>
						<tr>
							<td><button id="delete4" value="4">Delete</button></td>
							<td><button id="delete5"value="5">Delete</button></td>
							<td><button id="delete6" value="6">Delete</button></td>

						</tr>
						<?php };?>

					</table>
<!-- Table zobrazující obrázky a pro Admina tlačítka delete END   -->

<!-- Pagination - výběr aktuální stránky   -->

					<ul class="pagination">
						<li><a id="first" > &lt; &lt; </a></li>
						<li><a id="page1" class="active">1</a></li>
						<li><a id="page2" >2</a></li>
						<li><a id="page3" >3</a></li>
						<li><a id="page4" >4</a></li>
						<li><a id="page5" >5</a></li>
						<li><a id="page6" >6</a></li>
						<li><a id="last" > &gt;&gt;</a></li>
					</ul>
<!-- Pagination - výběr aktuální stránky END  -->
		
			</div>




		</div>
		

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="scripts.js" ></script>
	<footer>&copy; 2016 Jaroslav Jandourek 	CVUT FEL-SIT</footer>
	</body>

</html>

